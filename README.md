模型驱动的云应用智能化编排部署工具整体采用MSA架构（microservice
architecture）设计实现，基于服务网格的微服务应用的编排拓扑描述模型SMTM模型（Service Mesh-based Topology Model），提供云应用的智能化编排功能，此外还提供包括应用管理、构件管理、镜像容器管理和服务治理等一站式管理功能。


## 目录

- [目录](#目录)
- [特性](#特性)
- [架构](#架构)
- [要求](#要求)
- [安装及运行](#安装及运行)
  - [基础软件安装](#基础软件安装)
  - [打包编译](#打包编译)
  - [部署war包到tomcat上](#部署war包到tomcat上)
  - [运行tomcat](#运行tomcat)
- [页面展示](#页面展示)
- [联系我们](#联系我们)

## 特性

- 简单易用
  - 可视化服务编排
  
- 服务化
  - 以服务的形式作为PaaS平台的主要功能，平台用户(应用提供方)只需要使用该服务，就能够完成应用在PaaS平台的部署任务

- 通用化
  - 与云平台的底层实现细节分离，用户不需要学习和关注平台相关的语言和规范；另一方面，云应用智能部署服务可以与大多数云平台集成使用，具有平台通用性
  
- 流程化
  - 使用户更关注所期望的系统部署目标状态与部署任务流程的描述和设计，而不是命令式的、细节的部署任务脚本编写

- 模型化
  - 系统提供建模的技术支撑用户编排服务的过程

- 智能化
  - 系统提供智能化加速技术如编排推荐，提升用户编排部署的效率


## 架构
![](./doc/arch.png)
## 要求
* JDK 1.8
* Apache Maven 3.5.0
* MySQL 5.6.44 
* Apache Tomcat 8.5.43
* Docker 1.9
* Kubernates 1.11.2

## 安装及运行
### 基础软件安装
    首先需要安装基础软件JDK, Maven, MySQL, Tomcat, Docker, Kubernates
### 打包编译
    mvn clean package -Dmaven.test.skip=true
### 部署war包到tomcat上
    将cloudapp/target/cloudapp.war复制到tomcat的${TOMCAT_HOME}/webapps目录下
### 运行tomcat
    cd ${TOMCAT_HOME}
    ./bin/startup.sh

## 页面展示
- `模型驱动编排`:

  ![](./doc/demo1.png)
  
- `编排推荐示例`:

  ![](./doc/demo2.png)
  
- `大规模微服务的编排推荐`:

  ![](./doc/demo3.png)
  
- `服务监控`:

  ![](./doc/demo4.png)
  
  
  
## 联系我们
- Name: 陈老师  
- Email:  wchen@otcaix.iscas.ac.cn